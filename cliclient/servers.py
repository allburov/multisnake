import json

from tornado.httpclient import HTTPClient, AsyncHTTPClient


class IServer(object):
    async def get_data(self):
        raise NotImplementedError()

    async def send_key(self, key):
        raise NotImplementedError()

    @property
    def help(self):
        raise NotImplementedError()


class RemoteServer(IServer):
    def __init__(self, url, game_name, snake_name, **kwargs):
        self.url = url
        self.client = AsyncHTTPClient()
        self.game_name = game_name
        self.snake_name = snake_name
        self.tick_number = 0
        self.game_data_url = self.url + "/api/games/{}".format(self.game_name)
        self._connect_or_create(**kwargs)

    def _connect_or_create(self, **kwargs):
        """
        Create game if does not exist
        :param kwargs:
        :return:
        """
        r = HTTPClient().fetch(self.game_data_url, raise_error=False)
        if r.code == 404:
            if kwargs is None:
                kwargs = {}
            kwargs['name'] = self.game_name
            HTTPClient().fetch(self.url + "/api/games", method="POST",
                               body=json.dumps(kwargs))

    def __str__(self):
        return "RemoteServer: server-url: {}, game: {}, nickname={}".format(self.url, self.game_name, self.snake_name)

    @property
    def help(self):
        return """Control keys: UP, DOWN, LEFT, RIGHT
Your friend can connect to server {} and game '{}'.
Scores:
- @ - Food: 10
- X - Wall, or other Snake: -50
- Snake moved: 1
""".format(self.url, self.game_name)

    async def get_data(self):
        response = await self.client.fetch(self.game_data_url + "?tick={}".format(self.tick_number))
        response_json = json.loads(response.body.decode('utf-8'))
        self.tick_number = response_json['tick']

        screen = {}
        screen_raw = response_json['screen']
        for xy, value in screen_raw.items():
            x, y = map(int, xy.split(','))
            screen[(x, y)] = value
        response_json['screen'] = screen
        return response_json

    async def send_key(self, key):
        if key in ('up', 'down', 'left', 'right'):
            data = {
                'name': self.snake_name,
                'key': key
            }
            await self.client.fetch(self.game_data_url, method="POST", body=json.dumps(data))


class LocalGame(IServer):
    KEY_SECOND_PLAYER_MAP = {
        'w': 'up',
        's': 'down',
        'a': 'left',
        'd': 'right',
    }

    def __init__(self, **kwargs):
        from server.model.game import SnakeGame
        self.game = SnakeGame(**kwargs)

    def __str__(self):
        return "LocalGame"

    async def get_data(self):
        await self.game.tick()
        return self.game.dump()

    async def send_key(self, key):
        if key in ('up', 'down', 'left', 'right'):
            self.game.snake_change_direction('Snake1', key)

        if key in ('w', 'a', 's', 'd'):
            key = self.KEY_SECOND_PLAYER_MAP[key]
            self.game.snake_change_direction('Snake2', key)

    @property
    def help(self):
        return """You can play with a friend in one computer!  
Control keys:
- Snake1: UP, DOWN, LEFT, RIGHT
- Snake2: W, S, A, D
Scores:
- @ - Food: 10
- X - Wall, or other Snake: -50
- Snake moved: 1
"""
