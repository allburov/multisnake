# TODO: Сделать клиента асинхронного - нажатие, загрузка в разных IO
import argparse
import asyncio
import os
import random
import signal
# Catch CTRL+V and exit
from asyncio import FIRST_COMPLETED, Event
from math import sqrt

import colorama

from cliclient.servers import RemoteServer, LocalGame, IServer

signal.signal(signal.SIGINT, signal.SIG_DFL)

import keyboard

clearscreen = lambda: os.system('cls') if os.name == 'nt' else os.system('clear')


class MultiSnakeClient(object):
    COLOR_MAP = {
        0: ' ',
        1: colorama.Fore.GREEN + "#" + colorama.Fore.RESET,
        2: colorama.Fore.YELLOW + '*' + colorama.Fore.RESET,
        10: colorama.Fore.CYAN + "@" + colorama.Fore.RESET,
        100: colorama.Fore.RED + "Х" + colorama.Fore.RESET,
    }

    def __init__(self, server=None):
        self.screen = {}
        self.printed_screen = {}
        self.scores = []
        self.end_score = 0
        self.server = server  # type: IServer
        self.stub = None
        self.print_screen_event = Event()
        self.finished = False

    async def get_server_data(self):
        while True:
            data = await self.server.get_data()
            self.screen = data['screen']
            self.scores = data['scores']
            self.end_score = data['end_score']
            self.finished = data['finished']
            self.print_screen_event.set()

    @property
    def header(self):
        header = "**********************************************\n" + \
                 "Multisnake cli-client (CTRL+C to exit)\n" + \
                 "- {}\n".format(self.server) + \
                 "**********************************************\n" + \
                 'HELP:\n' + \
                 "Snakes will create after first pressed key.\n" + \
                 self.server.help + \
                 "\n**********************************************"
        return header

    def print_score(self):
        print("Target score: {score} or -{score}".format(score=self.end_score))
        for snake in self.scores:
            print("***********")
            if snake['winner']:
                print("{}: WINNER!!!!".format(snake['name']))
            else:
                print("{}:".format(snake['name']))
            color = self.COLOR_MAP.get(snake['color'], snake['color'])
            print("Color: {}".format(color))
            print("Score: {}".format(snake['score']))

    async def print_screen(self):
        await self.print_screen_event.wait()
        self.print_screen_event.clear()

        print(self.header)
        self.print_square()
        while True:
            if self.finished:
                break
            else:
                await self.print_screen_event.wait()
                self.print_screen_event.clear()
            self.update_square()
            # TODO: Вернуть принт Score
            # self.print_score()

    def pos(self, x, y):
        res = '\x1b[%d;%dH' % (y + 2 + len(self.header.splitlines()), x + 2)
        return res

    def update_square(self):
        """
        Обновляем игровую площать
        Чтобы убрать мерцания экрана обновляем точки по координадам
        Сохраняем распечатанный экран - чтобы изменять только измененные точки
        :return:
        """
        width = int(sqrt(len(self.screen)))
        for i in range(width):
            for j in range(width):
                point = (j, i)
                if self.screen[point] != self.printed_screen.get(point):
                    print(self.pos(*point) + self.COLOR_MAP.get(self.screen[point], self.screen[point]), end='')
        self.printed_screen = self.screen

    def print_square(self):
        width = int(sqrt(len(self.screen)))
        print((width + 2) * "-")
        for i in range(width):
            print("|", end='')
            print(' ' * width, end='')
            print("|")
        print((width + 2) * "-")

    async def keys(self):
        while True:
            await self.key_pressed(('up', 'down', 'left', 'right'))
            await self.key_pressed(('w', 'a', 's', 'd'))
            await asyncio.sleep(0.1)

    async def key_pressed(self, keys):
        key = next((k for k in keys if keyboard.is_pressed(k)), None)
        if key is not None:
            await self.server.send_key(key)


def parse_args():
    parser = argparse.ArgumentParser()

    parser.add_argument("--server-url", action="store", help="Specify server URL or not (for local single gaming)")
    parser.add_argument("--game", action="store",
                        help="Specify Game name, where your want connect (will be created if not exist)")
    parser.add_argument("--name", action="store", help="Your name")
    parser.add_argument("--width", action="store", default=30, type=int)
    parser.add_argument("--speed", action="store", default=0.3, type=float)
    parser.add_argument("--end-score", action="store", default=600, type=int)
    args = parser.parse_args()
    return args


async def amain(multisnake):
    tasks = [multisnake.keys(), multisnake.get_server_data(), multisnake.print_screen()]
    done, pending = await asyncio.wait(tasks, return_when=FIRST_COMPLETED)
    # cancel any pending tasks, the tuple could be empty so it's safe
    for pending_task in pending:
        pending_task.cancel()

    # process the done tasks
    for done_task in done:
        done_task.result()


def main():
    colorama.init()
    clearscreen()
    args = parse_args()
    if args.game is None:
        args.game = "snake{}".format(random.randint(0, 100))

    if args.server_url is not None:
        server = RemoteServer(args.server_url, args.game, args.name,
                              width=args.width, speed=args.speed, end_score=args.end_score)
    else:
        server = LocalGame(width=args.width, speed=args.speed, end_score=args.end_score)

    multisnake = MultiSnakeClient(server)
    loop = asyncio.get_event_loop()
    loop.run_until_complete(amain(multisnake))


if __name__ == "__main__":
    main()
