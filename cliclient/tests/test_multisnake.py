from cliclient.multisnake import MultiSnakeClient


def test_square(capsys):
    test_sq = [
        [0, 0, 0, 0],
        [1, 0, 0, 0],
        [0, 0, 2, 0],
        [10, 0, 0, 100],

    ]
    test_sq = {
        (0, 0): 0,
        (0, 1): 0,
        (0, 2): 0,
        (0, 3): 0,
        (1, 0): 1,
        (1, 1): 0,
        (1, 2): 0,
        (1, 3): 0,
        (2, 0): 0,
        (2, 1): 0,
        (2, 2): 2,
        (2, 3): 0,
        (3, 0): 10,
        (3, 1): 0,
        (3, 2): 0,
        (3, 3): 100,
    }
    expected = """------
|    |
|#   |
|  * |
|@  Х|
------
"""
    multisnake = MultiSnakeClient()
    multisnake.screen = test_sq
    multisnake.print_square()
    captured = capsys.readouterr()
    assert expected == captured.out
