import asyncio
import datetime
import random
from asyncio import Event, Condition

from server.model.objects import Point, Direction, Emtpty, Food, SnakeFactory, Wall, Snake


class TickedEvent(Event):
    def __init__(self, tick):
        self.tick = tick
        super(TickedEvent, self, ).__init__()


class SnakeGame(object):
    """
    Main object, e.g. Manager for all GameObjects
    """

    def __init__(self, width, speed, food_divider=3, wall_divider=5, end_score=None, name=None):
        self.speed = speed
        self.width = width
        self.name = name
        if end_score:
            self.end_score = end_score
        else:
            self.end_score = self.width * 20
        self.tick_number = 1
        self.finished = False
        self.new_data = Condition()
        self.time_start = datetime.datetime.now()
        self.time_end = None

        self._empty = Emtpty()
        self._screen = {Point(x, y, width): self._empty for x in range(width) for y in range(width)}
        self._objects = []
        self._snake_factory = SnakeFactory()
        self._snakes = {}

        if food_divider is not None:
            for i in range(width // food_divider):
                food = Food(self._random_point())
                self._objects.append(food)

        if wall_divider is not None:
            for i in range(width // wall_divider):
                wall = Wall(self._random_point())
                self._objects.append(wall)

    async def tick(self):
        """
        Game "tick" event - resend them to all object
        :return:
        """
        await asyncio.sleep(self.speed)
        for obj in self._objects:
            obj.tick(self)
        self._update()
        self.tick_number += 1
        self.game_over()
        async with self.new_data:
            self.new_data.notify_all()
        return self.finished

    def game_over(self):
        """
        Rules when game is over
        :return:
        """
        for name, snake in self._snakes.items():
            if abs(snake.score) > self.end_score:
                snake.winner = True
                self.finished = True
                self.time_end = datetime.datetime.now()

    def dump(self):
        """
        Main game represantation - full dump all cells
        :return:
        """
        scores = [snake.dump() for snake in self._snakes.values()]
        data = {
            "name": self.name,
            "width": self.width,
            "speed": self.speed,
            "finished": self.finished,
            "screen": {(point.x, point.y): obj.color for point, obj in self._screen.items()},
            "tick": self.tick_number,
            "scores": scores,
            "end_score": self.end_score,
            "start": self.time_start,
            "end": self.time_end,

        }
        return data

    def snake_change_direction(self, snake_name, direction):
        """
        Change snakes direction and create new snake if not exist
        :param snake_name:
        :param direction:
        :return:
        """
        if self._snakes.get(snake_name, None) is None:
            snake = self._event_add_snake(snake_name)
            self._snakes[snake_name] = snake
        snake = self._snakes[snake_name]
        snake.direction = direction

    def event_snake_moved(self, snake, point):
        """
        Some snake want move to point
        :param snake:
        :param point:
        :return:
        """
        obj = self._screen[point]
        if isinstance(obj, Food):
            self._event_food_replace(obj)
            snake.eat(point)
        elif isinstance(obj, Wall):
            snake.respawn(self._random_point())
        elif isinstance(obj, Snake):
            if obj is snake:
                snake.respawn(self._random_point())
            else:
                snake.respawn(self._random_point())
        else:
            snake.move(point)

    def _random_point(self):
        """
        Выбираем первую свободную точку в игровом поле
        :return:
        """
        point = None
        while point is None or self._screen[point] != self._empty:
            x = random.randint(0, self.width - 1)
            y = random.randint(0, self.width - 1)
            point = Point(x, y, self.width)
        return point

    def _update(self):
        """
        Update Game after screen
        :return:
        """
        for point in self._screen.keys():
            self._screen[point] = self._empty
        for obj in self._objects:
            for point in obj.positions:
                self._screen[point] = obj

    def _event_food_replace(self, food):
        """
        Replace eated Food
        :param food:
        :return:
        """
        self._objects.remove(food)
        food = Food(self._random_point())
        self._objects.append(food)

    def _event_add_snake(self, snake_name):
        """
        Add a new snake to the game
        :param point:
        :return: snake object
        """
        point = self._random_point()
        snake = self._snake_factory.get(point, Direction('up'), name=snake_name)
        self._objects.append(snake)
        return snake
