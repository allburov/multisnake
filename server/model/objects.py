from collections import namedtuple

# Не делается отдельный класс, т.к. операция add нужна в одном-двух местах,
# а новый класс сложно использовать в качестве ключей в dict, нужен либо flyiweight patter или что-то хитрое
Point = namedtuple("Point", ('x', 'y', 'mod'))


def add_p(p1, p2):
    if p1.mod is None:
        return Point(
            x=p1.x + p2.x,
            y=p1.y + p2.y,
            mod=None
        )
    else:
        return Point(
            x=(p1.x + p2.x) % p1.mod,
            y=(p1.y + p2.y) % p1.mod,
            mod=p1.mod
        )


class Direction(object):
    MAP = {
        'up': Point(0, -1, None),
        'down': Point(0, 1, None),
        'right': Point(1, 0, None),
        'left': Point(-1, 0, None),
    }

    def __init__(self, direction):
        self.direction = direction

    def inc(self):
        return self.MAP[self.direction]


class GameObject(object):
    def __init__(self, color):
        self.color = color

    def tick(self, game):
        pass


class Emtpty(GameObject):
    """
    Просто пустое поле
    """
    COLOR = 0

    def __init__(self):
        super(Emtpty, self).__init__(self.COLOR)


class Snake(GameObject):
    FOOD_SCORE = 10
    RESPAWN_SCORE = -50
    MOVE_SCORE = 1

    def __init__(self, start, direction: Direction, name, color):
        super(Snake, self).__init__(color)
        self.positions = [start]
        self.name = name
        self.winner = False
        self.__direction = direction
        self._score = 0

    def dump(self):
        return {"name": self.name, "color": self.color, "score": self.score, "winner": self.winner}

    @property
    def score(self):
        return self._score

    def respawn(self, point):
        self.positions = [point]
        self._score += self.RESPAWN_SCORE

    def tick(self, game):
        point = add_p(self.positions[-1], self.direction.inc())
        game.event_snake_moved(snake=self, point=point)

    def move(self, point):
        self.positions.append(point)
        self.positions.pop(0)
        self._score += self.MOVE_SCORE

    def eat(self, point):
        """
        Eat next point
        :param point:
        :return:
        """
        self.positions.append(point)
        self._score += self.FOOD_SCORE

    # TODO: not used, remove or use
    def eated(self, point):
        """
        Snake was eated
        :param point:
        :return:
        """
        index = self.positions.index(point)
        self.positions = self.positions[:index]

    @property
    def direction(self):
        return self.__direction

    @direction.setter
    def direction(self, direction):
        # check direction value
        if direction is None:
            pass
        elif isinstance(direction, Direction):
            pass
        else:
            # str to object
            direction = Direction(direction)

        # Snake can't reverse
        if add_p(direction.inc(), self.__direction.inc()) == Point(0, 0, None) and len(self.positions) > 1:
            return

        # change direction
        self.__direction = direction


class TooManySnakesException(Exception):
    pass


class SnakeFactory:
    def __init__(self):
        self.snakes_color = [0]

    def get(self, *args, **kwargs):
        color = self.snakes_color[-1] + 1
        if color >= 10:
            return None
        self.snakes_color.append(color)
        snake = Snake(*args, **kwargs, color=color)
        return snake


class Food(GameObject):
    """
    Омноном
    """
    COLOR = 10

    def __init__(self, point):
        self.positions = [point]
        super(Food, self, ).__init__(self.COLOR)


class Wall(GameObject):
    COLOR = 100

    def __init__(self, point):
        self.positions = [point]
        super(Wall, self, ).__init__(self.COLOR)
