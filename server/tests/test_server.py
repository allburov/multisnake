from tornado.testing import AsyncHTTPTestCase

from cliclient.servers import RemoteServer
from server.multisnaked import make_app


class TestSmoke(AsyncHTTPTestCase):
    def get_app(self):
        return make_app()

    def test_0_homepage(self):
        response = self.fetch('/')
        self.assertEqual(200, response.code)

    def test_load_add_1000_games(self):
        for i in range(1000):
            server = RemoteServer("http://localhost:8888/", str(i), snake_name=str(i))
            print("game {} created".format(i))
