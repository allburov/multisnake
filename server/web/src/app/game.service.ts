import { Injectable } from '@angular/core';
import { MessageService } from './message.service';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Game } from './game';
import { catchError, map, tap } from 'rxjs/operators';
import { Point } from './point';




@Injectable({
  providedIn: 'root'
})
export class GameService {

  constructor(
    private http: HttpClient,
    private messageService: MessageService,
  ) { }
  /**
* Handle Http operation that failed.
* Let the app continue.
* @param operation - name of the operation that failed
* @param result - optional value to return as the observable result
*/
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
  private gamesUrl = 'api/games'

  private log(message: string) {
    this.messageService.add(`GameService: ${message}`);
  }

  getGamees(): Observable<Game[]> {
    return this.http.get<Game[]>(this.gamesUrl)
      .pipe(
        tap(_ => this.log('fetched Gamees')),
        catchError(this.handleError('getGamees', [])))
  }

  /** GET Game by id. Will 404 if id not found */
  getGame(id: string): Observable<Game> {
    const url = `${this.gamesUrl}/${id}`;
    return this.http.get<Game>(url).pipe(
      map(this.gameFromJson),
      tap(_ => this.log(`fetched Game id=${id}`)),
      catchError(this.handleError<Game>(`getGame id=${id}`))
    );
  }

  gameFromJson(game: Game) {
    var points = [];
    let screen: Map<string, number> = game.screen
    for (let value in screen){
      let xy = value.split(',')
      let x = parseInt(xy[0])
      let y = parseInt(xy[1])
      let color = screen[value]
      let point: Point = {
        x: x,
        y: y,
        color: color,
      }
      points.push(point)

    }
    game.screen = points;
    return game

  }
}

