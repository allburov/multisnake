import { Component, OnInit } from '@angular/core';
import { GameService } from '../game.service';
import { Game } from '../game'
import { Point } from '../point'
import { element } from '@angular/core/src/render3';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit {

  constructor(
    private gameService: GameService,
  ) { }
  private game: Game;
  public width: number[];

  ngOnInit() {
    this.getGame('mock')
  }

  getGame(id: string): void {
    this.gameService.getGame(id)
      .subscribe(game => {
        this.game = game;
        this.width = Array(this.game.width)
      });
  }
  private COLORS = {
    0: '#000',
    1: '#0DFF00',
    2: '078F00',
    10: '#008cff',
    100: '#E80505'
  };
  public hex(point): string {
    return this.COLORS[point.color] || "#{{this.color}}";
  }
  public foundhex(x, y): string {
    let point = this.game.screen.find(
      (element, index, array) => {
        return (element.x === x && element.y === y)
      })
    return this.hex(point)
  }
}
