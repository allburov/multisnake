import { MapType } from '@angular/compiler';
import { Player } from './player';
import { Point } from './point';

export class Game {
    width: number;
    name: string;
    speed: number;

    start: string;
    end: string;
    finished: boolean;
    tick: number;

    scores: Player[];
    screen: Point[];
}