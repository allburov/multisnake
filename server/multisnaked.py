import asyncio
import datetime
import json
import logging

import tornado.escape
import tornado.ioloop
import tornado.web
from motor.motor_tornado import MotorClient
from tornado.log import enable_pretty_logging

from server.model.game import SnakeGame

enable_pretty_logging()

GAMES = {}


# TODO: Отрефакторить в стиле tornado, motor
# TODO: Унести конфигурацию mongodb в файл
async def everytime(game: SnakeGame):
    while True:
        finished = await game.tick()
        if finished:
            logging.info("Game Over: {}".format(game.name))
            db_scores = MotorClient("192.168.56.101", 27017).multisnake.scores
            game_data = game.dump()
            game_data.pop('screen')
            await asyncio.sleep(10)
            result = await db_scores.insert_one(game_data)
            logging.info("Saved with _id: {}".format(result.inserted_id))
            break


class MainHandler(tornado.web.RequestHandler):
    async def get(self):
        pass


class GamesManagement(tornado.web.RequestHandler):
    async def get(self):
        self.write({"games": list(GAMES.keys())})

    async def post(self):
        data = tornado.escape.json_decode(self.request.body)
        if data.get('name', False):
            name = data['name']
            width = int(data.get('width', 40))
            speed = float(data.get('speed', 0.3))
            end_score = int(data.get('end_score', None))
            game = SnakeGame(width, speed, name=name, end_score=end_score)
            GAMES[name] = game
            tornado.ioloop.IOLoop.current().spawn_callback(everytime, game)
            logging.info("Game started: {}".format(game.name))
            self.write({'name': name})
            self.set_status(201)
        else:
            raise tornado.web.HTTPError(400, 'Specify name field')


def json_datetime(o):
    if isinstance(o, datetime.datetime):
        return o.__str__()


class GameControl(tornado.web.RequestHandler):
    async def get(self, name):
        if name not in GAMES:
            self.set_status(404)
            self.write('Game with name {} not found'.format(name))
            return

        game = GAMES[name]  # type: SnakeGame
        tick = int(self.get_argument('tick', 0))
        async with game.new_data:
            await game.new_data.wait()
        game_data = game.dump()
        screen = {"{},{}".format(p[0], p[1]): value for p, value in game_data['screen'].items()}
        game_data['screen'] = screen

        data_str = json.dumps(game_data, default=json_datetime).replace("</", "<\\/")
        self.set_header("Content-Type", "application/json")
        self.write(data_str)

    async def post(self, name):
        game = GAMES[name]  # type: SnakeGame
        data = tornado.escape.json_decode(self.request.body)
        key = data['key']
        snake = data['name']
        game.snake_change_direction(snake, key)


def make_app():
    return tornado.web.Application([
        (r"/api/", MainHandler),
        (r"/api/games", GamesManagement),
        (r"/api/games/([^/]+)", GameControl),
    ],
        # debug=True
    )


if __name__ == "__main__":
    app = make_app()
    app.listen(8888)
    tornado.ioloop.IOLoop.current().start()
